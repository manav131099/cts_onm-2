rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .005
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
	TIMESTAMPSALARM <<- NULL
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	if(nrow(dataread) < 1)
	{
	  print('Err in file')
		return(NULL)
	}
	dataread2 = dataread
	dataread = dataread2[complete.cases(as.numeric(dataread2[,39])),]
	{
	if(nrow(dataread) < 1)
	{
	 Eac2 = 0
	}
	else{
	Eac2 = format(round(((as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]))/1000),2),nsmall=2)
	}
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,15])),]
  {
	if(nrow(dataread) < 1){
	 Eac1 = 0}
	else{
	Eac1 = format(round(sum(as.numeric(dataread[,15]))/60000,2),nsmall=1)
	}
	}
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 479,]
  tdx = tdx[tdx > 479]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,15])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,15]) < ltcutoff*1000,]
	if(length(dataread2[,1]) > 0)
	{
			TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
	dspy = round(as.numeric(Eac1)/100.8,2)
	dspy2 = round(as.numeric(Eac2)/100.8,2)
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
  df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield=dspy,DailySpecYield2=dspy2,stringsAsFactors=F)
  write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t")
  Eac1T = as.numeric(dataread1[,2])
  Eac2T = as.numeric(dataread1[,3])
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),EacTotMethod1 = Eac1T,EacTotMethod2=Eac2T,stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
  return(c(as.numeric(Eac1T),as.numeric(Eac2T)))
}

