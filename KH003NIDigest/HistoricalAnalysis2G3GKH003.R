system('rm -R "/home/admin/Dropbox/Second Gen/[KH-003S]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/KH003NIDigest/KH003MailDigestFunctions.R')

path = '/home/admin/Dropbox/Cleantechsolar/1min/[715]'
writepath2G = '/home/admin/Dropbox/Second Gen/[KH-003S]'
checkdir(writepath2G)

writepath3G = writepath2G
DAYSALIVE = 0
years = dir(path)
for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
		filenam = paste(substr(as.character(years[x]),3,4),substr(as.character(months[y]),6,7),sep="")
    writepath3Gfinal = paste(writepath2Gmon,"/[KH-003S] ",filenam,".txt",sep="")
    days = dir(pathmonths)
      for(t in 1 : length(days))
      {
        {
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
        }
        DAYSALIVE = DAYSALIVE + 1
				currdayw = gsub("715","KH-003S",days[t])

        writepath2Gfinal = paste(writepath2Gmon,"/",currdayw,sep="")
        readpath = paste(pathmonths,days[t],sep="/")
        df1 = secondGenData(readpath,writepath2Gfinal)
        tots = thirdGenData(writepath2Gfinal,writepath3Gfinal)
      }
    }
}
print('Historical Analysis Done')
