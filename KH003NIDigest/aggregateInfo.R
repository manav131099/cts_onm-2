source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-003S",c("Coke","Solar","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 14 #column for LastRead
aggColTemplate[4] = 13 #column for LastTime
aggColTemplate[5] = 10 #column for Eac-1
aggColTemplate[6] = 11 #column for Eac-2
aggColTemplate[7] = 50 #column for Yld-1
aggColTemplate[8] = 41 #column for Yld-2
aggColTemplate[9] = 52 #column for PR-1
aggColTemplate[10] = 53 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 26 #column for Tamb
aggColTemplate[14] = 16 #column for Tmod
aggColTemplate[15] = 18 #column for Hamb

registerColumnList("KH-003S","Coke",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 9 #column for LastRead
aggColTemplate[4] = 8 #column for LastTime
aggColTemplate[5] = 5 #column for Eac-1
aggColTemplate[6] = 6 #column for Eac-2
aggColTemplate[7] = 46 #column for Yld-1
aggColTemplate[8] = 47 #column for Yld-2
aggColTemplate[9] = 48 #column for PR-1
aggColTemplate[10] = 49 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 26 #column for Tamb
aggColTemplate[14] = 16 #column for Tmod
aggColTemplate[15] = 18 #column for Hamb

registerColumnList("KH-003S","Solar",aggNameTemplate,aggColTemplate)

