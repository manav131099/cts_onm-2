rm(list=ls(all =TRUE))
library(ggplot2)
pathRead <- "~/intern/data/[714]/2017"
setwd(pathRead)

filelist <- dir(pattern = ".txt", recursive= TRUE)

rf = function(x){
  return(format(round(x,3),nsmall=3))
}

#Peak condition 1 
#Condition 1; defining peak time
timemin <- format(as.POSIXct("2017-01-07 08:59:00"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-01-07 22:01:00"), format="%H:%M:%S")

#Public holidays obs in 2017 [Thailand] 
#Condition 2; defining off peak days on week days)
  PH0 <- as.Date("2017-01-01")    #new year day
  PH1 <- as.Date("2017-01-02")    #new year day obs
  PH2 <- as.Date("2017-02-11")    #Makha Bucha
  PH3 <- as.Date("2017-04-06")    #Chakri day
  PH4 <- as.Date("2017-04-13")    #Songkran
  PH5 <- as.Date("2017-04-14")    #Songkran 2
  PH6 <- as.Date("2017-04-15")    #Songkran 3
  PH7 <- as.Date("2017-04-17")    #Songkran obs
  PH8 <- as.Date("2017-05-01")    #Labour day
  PH9 <- as.Date("2017-05-10")    #Visakha Bucha
  PH10 <- as.Date("2017-05-12")   #Royal ploughing ceremony
  PH11 <- as.Date("2017-07-09")   #Buddhist Lent Day
  PH12 <- as.Date("2017-07-10")   #Asaalha Bucha
  PH13 <- as.Date("2017-07-28")   #King's Bday
  PH14 <- as.Date("2017-08-12")   #Queen's bday
  PH15 <- as.Date("2017-08-14")   #Queen's bday obs
  PH16 <- as.Date("2017-10-13")   #Anniversary of Death of King Bhumibol
  PH17 <- as.Date("2017-10-23")   #Chulalongkorn Day
  PH18 <- as.Date("2017-10-26")   #Cremation of King Bhumibol
  PH19 <- as.Date("2017-12-10")   #Constitution Day
  PH20 <- as.Date("2017-12-11")   #Constitution Day obs
  PH21 <- as.Date("2017-12-31")   #New year's eve
  PH22 <- as.Date("2017-12-05")   #Father's day
  
#phlist <- c(PH1,PH2,PH3,PH4,PH5,PH6,PH7,PH8,PH9,PH10,PH11,PH12,PH13,PH14,PH15)

#dates <- seq.Date(as.Date("2017-01-01"),as.Date("2017-12-31"),by="1 day")

#dates[weekdays(dates) == "Saturday" | weekdays(dates) == "Sunday"]
#seq_days = as.Date(dates)
#num_days = format(dates, "%w")
#condition2 <- dates != PH1 & dates != PH2 
#condition4 <- num_days != 6 & num_days != 0
#peak <- dates[condition2 & condition4]
#peak_time <-format(as.POSIXct(peak), format='%Y-%m-%d: %H:%M:%S')

i=0
df=dff=NULL
for(z in filelist[1:length(filelist)]){
  temp <- read.table(z, header =T, sep= '\t', stringsAsFactors = F)
  condition1 <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin & format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  total_gsi <- as.numeric(rf(sum(abs(temp[,3]), na.rm = T)/60000))
  #total_eac <- as.numeric(rf(sum(abs(temp[,71]), na.rm = T)/60000))
  pac_TH <- (temp[,71]/63)*1404
  total_eac <- as.numeric(rf(sum(pac_TH, na.rm = T)/60000))
  pacTemp <- temp[condition1,]
  
  date=as.Date(temp[1,1])
  if(date == PH1 | date == PH2 | date == PH3 | date == PH4 | date == PH5 | date == PH6 | date == PH7 |date == PH8 | date == PH9 | date == PH10 |
     date == PH11 | date == PH12 | date == PH13 | date == PH14 | date == PH15 | date == PH16 | date == PH17 |date == PH18 | date == PH19 | date == PH20 | 
     date == PH21 | date == PH22) {
    nonpeak_gsi = total_gsi
    peak_gsi = peak_eac = 0
    
    nonpeak_eac= total_eac
    #peak_eac = 0
  } else{
    peak_gsi <- as.numeric(rf(sum(as.numeric(abs(pacTemp[,3])))/60000))
    nonpeak_gsi = total_gsi - peak_gsi
    pac_TH2 <- (pacTemp[,71]/63)*1404
    peak_eac <- as.numeric(rf(sum(pac_TH2, na.rm = T)/60000))
    nonpeak_eac = total_eac - peak_eac
  }
  d <- format(head(date), format="%w")
  
  #Condition 3 ; weekends = Off Peak
  #defining non peak period at 0 (sunday) and 6 (saturday)
  if(d == 0 | d == 6) {
    nonpeak_gsi = total_gsi
    peak_gsi = peak_eac = 0
    nonpeak_eac = total_eac
  }
  #powertotal column BW ; 71

  df <- cbind(as.character(date),peak_gsi, total_gsi, rf(nonpeak_gsi), peak_eac, total_eac, rf(nonpeak_eac))
  dff <- rbind(dff,df)
  print(paste(z, "done"))
}

dff=data.frame(dff)
colnames(dff) <- c("Date","Peak_Gsi", "Total_Gsi","Offpeak_Gsi","Peak_Eac","Total_Eac","Offpeak_Eac")

write.table(dff,paste0('~/intern/Results/[714]/1. result/[714] 2017 sorted data TH.txt'), row.names = FALSE, sep= "\t")

themesettings1 <- theme(plot.title = element_text(hjust = 0.5), axis.title.x = element_text(size=19), axis.title.y = element_text(size=19), 
                        axis.text = element_text(size=11), 
                        panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                        legend.justification = c(1, 1), legend.position = c(1, 1))

dff$Peak_Eac <- as.numeric(as.character(dff$Peak_Eac))
dff$Offpeak_Eac <- as.numeric(as.character(dff$Offpeak_Eac))
dff$Total_Eac <- as.numeric(as.character(dff$Total_Eac))


#bar chart of energy ac in different periods
#p1 <- ggplot(data=dff) + theme_bw()
#p1 <- p1 + geom_bar(aes(x= as.Date(Date), y = Total_Eac, colour = as.factor("orange2")), stat = "identity") #total eac
#p1 <- p1 + geom_bar(aes(x= as.Date(Date), y = Peak_Eac, colour = as.factor("red")), stat="identity")  #peak eac
#p1 <- p1 + geom_bar(aes(x= as.Date(Date), y = Offpeak_Eac, colour = as.factor("lightblue")), stat="identity") #Off peak eac
#p1 <- p1 + themesettings1 + coord_cartesian(ylim = c(0,15)) + ylab('Energy AC [kWh/m�]') + xlab('Date')
#p1 <- p1 + scale_colour_manual('Period', values = c("orange2", "red", "lightblue"), labels = c('Total Energy AC','Peak Energy AC','Off-Peak Energy AC')) 


p1 <- ggplot() + theme_bw()
#p1 <- p1 + geom_line(aes(x= as.Date(Date), y = Total_Eac, colour = as.factor("orange2"))) #total eac
p1 <- p1 + geom_line(data=dff, aes(x= as.Date(Date), y = Offpeak_Eac, colour = "Off-Peak")) #Off peak eac
p1 <- p1 + geom_line(data=dff, aes(x= as.Date(Date), y = Peak_Eac, colour = "Peak"))  #peak eac
p1 <- p1 + themesettings1 + coord_cartesian(ylim = c(0,9)) + ylab('Energy AC [kWh/m�]') + xlab('Date')
p1 <- p1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
p1 <- p1 + scale_colour_manual('Period', values = c("Peak"="red", "Off-Peak"="lightblue")) #,labels = c('Peak EAC','Off-Peak EAC')) 

p1


ggsave(paste0('~/intern/Results/[714]/1. result/TH EAC 2017.pdf'), p1, width =12, height=6)



