errHandle = file('/home/admin/Logs/LogsTH001Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/TH001XDigest/HistoricalAnalysis2G3GTH001.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/TH001XDigest/aggregateInfo.R')
initDigest = function(df,no)
{
print(ncol(df))
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Solar [Invoice meter]'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Solar [Check meter]'
		noac=""
	}
	else if(as.numeric(no)==3)
	{
		no2 = "Load"
		noac=""
	}
	else if(as.numeric(no)==4)
	{
		no2 = "Sensors (from TH-001C)"
		noac=""
	}
	}
	#ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1]),no2)
  body = paste(body,"\n\n_________________________________________\n\n")
	{
	if(as.numeric(no)==1|| as.numeric(no)==2)
	{
  	body = paste(body,"Eac",noac,"-1 [kWh]: ",as.character(df[,4]),sep="")
		body = paste(body,"\n\nEac",noac,"-2 [kWh]: ",as.character(df[,5]),sep="")
		body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character(df[,6]))
		body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character(df[,7]))
		body = paste(body,"\n\nPR-1 [%]:",df[,8])
		body = paste(body,"\n\nPR-2 [%]:",as.character(df[,9]))
		print(ncol(df))
		
  	body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,11]))
  	body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,10]))
		body = paste(body,"\n\nDowntime [%]:",as.character(df[,3]))
	}
	else if(as.numeric(no)==3)
	{
  	body = paste(body,"Eac",noac," [kWh]: ",as.character(df[,4]),sep="")
		body = paste(body,"\n\nGrid Availability [%]:",as.character(df[,5]))
		body = paste(body,"\n\nDowntime [%]:",as.character(df[,3]))
	}
	else if(as.numeric(no)==4)
	{
  	body = paste(body,"Total daily irradiation [kWh/m2]: ",as.character(df[,12]),sep="")
		body = paste(body,"\n\nMean Tmod [C]:",as.character(df[,14]))
		return(body)
	}
	}
		acpts = round(as.numeric(df[,2]) * 14.4)
  	body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,2]),"%)",sep="")
  return(body)
}

printtsfaults = function(TS,num,body)
{
  no = num
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Solar [Invoice meter]'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Solar [Check meter]'
		noac=""
	}
	else if(as.numeric(no)==3)
	{
		no2 = "Load"
		noac=""
	}
	}
	num = no2
	if(length(TS) > 1)
	{
		if(as.numeric(no)==3 || as.numeric(no)==4)
		{
		body = paste(body,"\n\n_________________________________________\n\n")
		body = paste(body,paste("Timestamps for",num,"where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		}
	}
	return(body)
}

sendMail = function(df1,df2,df3,pth1,pth2,pth3,pth5,invoiceData)
{
	d5 = read.table(pth5,header =T,sep="\t")
	datepth = as.character(Sys.Date())                                                 
	invoicePath <- paste('/tmp/[TH-001X] TH Invoicing summary ',datepth,'.txt',sep="") 
  filetosendpath = c(pth1,pth2,pth3,pth5,invoicePath)
	filenams = c()

  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	yrcmd = substr(currday,14,17)
	moncmd = substr(currday,14,20)
	command = paste("Rscript '/home/admin/CODE/JasonGraphs/TH-001X/TH-001X Eac grouping.R'",yrcmd,moncmd)
	system(command) 
	invoiceData = read.table(invoicePath,header=T,sep="\t",stringsAsFactors=F)

	rowuse = nrow(invoiceData)
	dateinv = as.character(invoiceData[,1])
	dateinvmtch = match(substr(currday,14,23),dateinv)
	if(is.finite(dateinvmtch))
		rowuse = dateinvmtch
	invoiceData = invoiceData[rowuse,]
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: Siam Construction Steel Co",sep="")
	body = paste(body,"\n\nLocation: Rayong, Thailand")
	body = paste(body,"\n\nO&M Code: TH-001")
	body = paste(body,"\n\nSystem Size: 1404 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: Trina Solar / TSM-PD14-325Wp / 4320")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60-10 / 18")
	body = paste(body,"\n\nSite COD: 2018-03-29")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE)+20)))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE)+20)/365,2)))
  datemtch = substr(as.character(df1[1,1]),1,10)
	idxmtch = match(datemtch,as.character(d5[,1]))
	body = paste(body,"\n\nArtificial Load [kWh]:",as.character(d5[idxmtch,24]))
	body = paste(body,"\n\nSolar contribution [%]:",as.character(d5[idxmtch,25]))
	body = paste(body,"\n\nDifference between last recorded values of meters [%]:",as.character(d5[idxmtch,26]))
	body = paste(body,"\n\nDifference between energy recorded by meters during the day [%]:",as.character(d5[idxmtch,27]))
	body = paste(body,"\n\nTotal daily Irradiation [kWh/m2]:",as.character(df1[1,12]))
	print(ncol(df1))
	body = paste(body,"\n\nPeak Eac [kWh]:",as.character(invoiceData[,8]))
	body = paste(body,"\n\nOffPeak Eac [kWh]:",as.character(invoiceData[,9]))
	body = paste(body,"\n\nPeak Eac [%]:",as.character(invoiceData[,10]))
	body = paste(body,"\n\nOffPeak Eac [%]:",as.character(invoiceData[,11]))
	body = paste(body,"\n\nDay:",as.character(invoiceData[,2]))
	body = paste(body,"\n\nHoliday [Y/N]:",as.character(invoiceData[,3]))
	body = paste(body,initDigest(df1,1))  #its correct, dont change
	body = paste(body,initDigest(df2,2))  #its correct, dont change
	body = paste(body,initDigest(df3,3))  #its correct, dont change
	body = paste(body,initDigest(df1,4))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,1,body)
  body = printtsfaults(TIMESTAMPSALARM2,2,body)
	print('2G data processed')
	body = gsub("\n ","\n",body)
	while(1)
	{
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [TH-001X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		print("Mail has some error")
		Sys.sleep(180)
		next
	}
	break
	}
	recordTimeMaster("TH-001X","Mail",substr(currday,14,23))
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("TH-001X","m")

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("TH-001X","m")
  recordTimeMaster("TH-001X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[TH-001X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			stations = stations[c(2,3,1)]
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      pathdays3 = paste(pathmonths,stations[3],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      checkdir(writepath2Gdays3)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      days3 = dir(pathdays3)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 print(paste('Processing',days3[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
         readpath3 = paste(pathdays3,days3[t],sep="/")
				 TOTSOLAR <<- 0
				 METERCALLED <<- 1  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 METERCALLED <<- 3 # its correct dont change
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 invoiceData = computeInvoice(readpath)
				thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath3Gfinal,invoiceData)
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,df3,writepath2Gfinal,writepath2Gfinal2,
	writepath2Gfinal3,writepath3Gfinal,invoiceData)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
