require('httr')
source('/home/admin/CODE/MasterMail/timestamp.R')
cleansedata = function(pth)
{
	dfall = read.table(pth,header = T,sep="\t")
	listofstrngs = c("DTVS-Oragadam-P1a","DTVS-Oragadam-P1b","DTVS-Oragadam-P2")
	listofdfs = list()
	for(t in 1 : length(listofstrngs))
	{
	  idxuse = which(as.character(dfall[,13]) %in% listofstrngs[t])
	  df = dfall[idxuse,]
    c2 = as.numeric(df[,17])
    c2idx = which(c2 %in% 0)
	  c1 = as.character(df[,19])
    c2 = as.character(df[,18])
    c3 = as.character(df[,17])
    if(length(c2idx) > 1)
	  {
	    c1 = as.character(df[-(c2idx),19])
      c2 = as.character(df[-(c2idx),18])
      c3 = as.character(df[-(c2idx),17])
    }
    listofdfs[[t]] = data.frame(Tm=c1,Pac=c2,Eac=c3)
		tmLast = c1[length(c1)]
	}
	recordTimeMaster("IN-014T","Gen1",as.character(tmLast))
	#df2 = data.frame(Tm=c1,Pac=c2,Eac=c3)
  return(listofdfs)
}
pathCentral = '/home/admin/Data/TORP Data/AllStations'

probePath = function(day)
{
	path = pathCentral
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	#checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	#checkDir(pathYr)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	{
		if(file.exists(pathFinal))
		{
			df = read.table(pathFinal,header = T,sep = "\t")
			return(df)
		}
		else
			return(NULL)
	}
}

fetchrawdata = function(day1,day2)
{
  	#fetch data from SERVER USING A POST-request
		masterData = probePath(day1)
		
		if(is.null(masterData))
		{
			return(masterData)
		}
		stnnames = as.character(masterData[,13])
		subst = which(stnnames %in% c("DTVS-Oragadam-P1a","DTVS-Oragadam-P1b","DTVS-Oragadam-P2"))
		if(length(subst))
		{
			vals2 = masterData[subst,] # extract only rows with MJ Logistics tag in column 13
			recordTimeMaster("IN-014T","FTPNewFiles",as.character(vals2[nrow(vals2),19]))
			return(vals2)
		}
		return(NULL)
}
