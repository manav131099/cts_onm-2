rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsMY002CMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/MY002CDigest/summaryFunctions.R')
source('/home/admin/CODE/MY002CDigest/Customer.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(3,1,2)

#source('/home/admin/CODE/IN023Digest/Historical.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/MY002CDigest/aggregateInfo.R')

METERNICKNAMES = c("WMS","MFM-1","MFM-2")

METERACNAMES = c("WMS","MFM1","MFM2")
checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}

DOB = "02-08-2018"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("MY-002C","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
	if(length(days) == length(ref))
		return(days)
	days2 = extractDaysOnly(days)
	daysret = unlist(rep(NA,length(ref)))
	idxmtch = match(days2,ref)
	daysret[idxmtch] = as.character(days)
	if(!(is.na(daysret[length(daysret)])))
		return(daysret)
	else
		return(days)
}
performBackWalkCheck = function(day)
{
	path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[MY-002C]"
	retval = 0
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	pathprobe = paste(path,yr,yrmon,sep="/")
	stns = dir(pathprobe)
	print(stns)
	idxday = c()
	for(t in 1 : length(stns))
	{
		pathdays = paste(pathprobe,stns[t],sep="/")
		print(pathdays)
		days = dir(pathdays)
		dayMtch = days[grepl(day,days)]
		if(length(dayMtch))
		{
			idxday[t] = match(dayMtch,days)
		}
		print(paste("Match for",day,"is",idxday[t]))
	}
	idxmtch = match(NA,idxday[t])
	if(length(unique(idxday))==1 || length(idxmtch) < 5)
	{
		print('All days present ')
		retval = 1
	}
	return(retval)
}
sendMail= function(pathall,path1Gall)
{
  filenams = c()
  body = ""
	body = paste(body,"Site Name: Denis Group, Mafipro",sep="")
	body = paste(body,"\n\nLocation: Taiping, Perak, Malaysia - 34600")
	body = paste(body,"\n\nO&M Code: MY-002")
	body = paste(body,"\n\nSystem Size:",sum(INSTCAP))
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: JINKO / 330 / 1275")
	body = paste(body,"\n\nModule Brand / Model / Nos: SMA / Solid-Q50 / 8")
	body = paste(body,"\n\nSite COD: 2018-08-02")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(daysAlive))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(daysAlive))/365,2)))
	
	bodyac = body
	body = ""
	TOTALGENCALC = 0
	MYLD = c()
	
	acname = METERNICKNAMES
	globMtYld = globMtVal = globMtPR = c()
	idxglobMtYld = 1
	GTI = NA
	for(t in 1 : length(pathall))
  {
	type = 0
	meteridx = t
	if(t <= (1+NOMETERS))
		type = 1
	path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+2],".txt",sep="")
	print("---------------------")
	print(path)
	print(filenams[t])
	print("---------------------")
	if(type == 1)
	{
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,acname[t])
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
	{
	if(t >= 2)
	{
	body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
	body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
	TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
	globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
	globMtPR[idxglobMtYld] = as.numeric(dataread[1,8])
	globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
	body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
	body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
	body = paste(body,"PR-1 [%]:",as.character(dataread[1,7]),"\n\n")
	body = paste(body,"PR-2 [%]:",as.character(dataread[1,8]),"\n\n")
	body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,9]),"\n\n")
	body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
	body = paste(body,"Grid Availability [%]:",as.character(dataread[1,11]),"\n\n")
	body = paste(body,"Plant Availability [%]:",as.character(dataread[1,12]))
	idxglobMtYld = idxglobMtYld + 1
	}
	else if( t == 1)
	{
		body = paste(body,"GHI [kWh/m^2]:",as.character(dataread[1,3]),"\n\n")
		GTI = as.numeric(dataread[1,3])
		body = paste(body,"Tmod [C]:",as.character(dataread[1,5]),"\n\n")
		body = paste(body,"Tmod solar hours [C]:",as.character(dataread[1,7]),"\n\n")
		body = paste(body,"Station DOB:",as.character(DOB),"\n\n")
		body = paste(body,"Days alive:",as.character(daysAlive),"\n\n")
		body = paste(body,"Years alive:",as.character(round(as.numeric(daysAlive)/365,2)))
	}
	}
	next
  }
	}
	addwarn = 0
	sddev = covar = NA
	bodyac = paste(bodyac,"\n\nSystem Full Generation [kWh]:",sum(globMtVal))
	bodyac = paste(bodyac,"\n\nGHI [kWh/m2]:",GTI)
	for(t in 1 : NOMETERS)
	{
		 	bodyac = paste(bodyac,"\n\nYield",acname[(t+1)],"[kWh/kWp]:",
			 as.character(globMtYld[t]))
		 	bodyac = paste(bodyac,"\n\nPR",acname[(t+1)],"[%]:",
			 as.character(globMtPR[t]))
	}
	globMtYld = globMtYld[complete.cases(globMtYld)]
	sdm = covarm = NA
	if(length(globMtYld))
	{
		sdm =round(sdp(globMtYld),3)
		covarm = round(sdm*100/mean(globMtYld),1)
	}
	bodyac = paste(bodyac,"\n\nStdev/COV Yields (Energy meters):",sdm,"/",covarm,"[%]")
	body = paste(bodyac,body,sep="")
	body = gsub("\n ","\n",body)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [MY-002C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
  sendCustomerInfo(path1Gall[1],path1Gall[2],path1Gall[3],paste(path4G,"[MY-002C]-lifetime.txt",sep="/"),
	sum(INSTCAP),daysAlive)
recordTimeMaster("MY-002C","Mail",currday)
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[MY-002C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[MY-002C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[MY-002C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[MY-002C]'

if(RESETHISTORICAL)
{
	command = paste("rm -rf",path2G)
	system(command)
	command = paste("rm -rf",path3G)
	system(command)
	command = paste("rm -rf",path4G)
	system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "MY-002C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-MFM2-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	months = dir(pathyr)
	for(y in 1 : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		stns = dir(pathmon)
		stns = stns[reorderStnPaths]
		dunmun = 0
		for(t in 1 : length(stns))
		{
			type = 1
			if(grepl("MFM",stns[t]))
				type = 0
			if(stns[t] == "WMS")
				type = 2
			pathmon1 = paste(pathmon,stns[t],sep="/")
		  days = dir(pathmon1)
		  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			if(!file.exists(path2Gmon1))
			{	
				dir.create(path2Gmon1)
			}

		if(length(days)>0)
		{
		for(z in 1 : length(days))
		{
			if(ENDCALL == 1)
				break
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon1,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon1,days[z],sep="/")
			if(RESETHISTORICAL)
			{
				secondGenData(pathfinal,path2Gfinal,type)
			}
			if(!dunmun){
			daysAlive = daysAlive+1
			}
			if(days[z] == stopDate)
			{
				ENDCALL = 1
				print('Hit end call')
				next
			}
		}
		}
		dunmun = 1
		if(ENDCALL == 1)
			break
	}
	if(ENDCALL == 1)
	break
}
if(ENDCALL == 1)
break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
	recipients = getRecipients("MY-002C","m")
	recordTimeMaster("MY-002C","Bot")
	years = dir(path)
	noyrs = length(years)
	path2Gfinalall = vector('list')
	path1Gfinalall = vector('list')
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		checkdir(path2Gyr)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			stns = dir(pathmon)
			if(length(stns) < 2)
			{
				print('Station sync issue.. sleeping for an hour')
				Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
				stns = dir(pathmon)
			}
			stns = stns[reorderStnPaths]
			for(t in 1 : length(stns))
			{
				newlen = (y-startmn+1) + (x-prevx)
				print(paste('Reset newlen to',newlen))
				type = 1
				if(grepl("MFM",stns[t]))
					type = 0
				if(stns[t] == "WMS")
					type = 2
			  pathmon1 = paste(pathmon,stns[t],sep="/")
			  days = dir(pathmon1)
			  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			chkcopydays = days[grepl('Copy',days)]
			if(!file.exists(path2Gmon1))
			{
			dir.create(path2Gmon1)
			}
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are'),idxflse)
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			days = days[complete.cases(days)]
			if(t==1)
				referenceDays <<- extractDaysOnly(days)
			else
				days = analyseDays(days,referenceDays)
			nodays = length(days)
			if(y > startmn)
			{
				z = prevz = 1
			}
			if(nodays > 0)
			{
			startz = prevz 
			if(startz > nodays)
				startz = nodays
			for(z in startz : nodays)
			{
				if(t == 1)
				{
					path2Gfinalall[[newlen]] = vector('list')
					path1Gfinalall[[newlen]] = vector('list')
				}
				if(is.na(days[z]))
					next
				pathdays = paste(pathmon1,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon1,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal,type)
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				if(is.na(days[z]))
				{
					print('Day was NA, do next in loop')
					next
				}
			  SENDMAILTRIGGER <<- 1
				if(t == 1)
				{
					splitstr =unlist(strsplit(days[z],"-"))
					daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
					print(paste("Sending",daysSend))
					if(performBackWalkCheck(daysSend) == 0)
					{
						print("Sync issue, sleeping for an 1 hour")
						Sys.sleep(3600) # Sync issue -- meter data comes a little later than
					   	             # MFM and WMS
					}
				}
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
				path1Gfinalall[[newlen]][t] = paste(pathmon1,days[z],sep="/")
				newlen = newlen + 1
				print(paste('Incremented newlen by 1 to',newlen))
			}
			}
			}
		}
	}
	if(SENDMAILTRIGGER)
	{
	print('Sending mail')
	print(paste('newlen is ',newlen))
	for(lenPaths in 1 : (newlen - 1))
	{
		if(lenPaths < 1)
			next
		print(unlist(path2Gfinalall[[lenPaths]]))
		pathsend = unlist(path2Gfinalall[[lenPaths]])
		path1GSend = unlist(path1Gfinalall[[lenPaths]])
		daysAlive = daysAlive+1
		sendMail(pathsend,path1GSend)
	}
	SENDMAILTRIGGER <<- 0
	newlen = 1
	print(paste('Reset newlen to',newlen))
	}

	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
