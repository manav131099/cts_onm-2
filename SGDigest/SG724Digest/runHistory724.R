rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
rf0 = function(x)
{
  return(format(round(x,0),nsmall=0))
}
prepareSumm = function(dataread)
{
  da = nrow(dataread)
  daPerc = round(da/14.4,1)
	thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,5]),5])/60000
  gsismp = sum(dataread[complete.cases(dataread[,3]),3])/60000
  subdata = dataread[complete.cases(dataread[,4]),]
  subdata = subdata[as.numeric(subdata[,4]) > thresh,]
  tamb = mean(dataread[complete.cases(dataread[,6]),6])
  tambst = mean(subdata[,6])
  
  hamb = mean(dataread[complete.cases(dataread[,7]),7])
  hambst = mean(subdata[,7])
  
  tambmx = max(dataread[complete.cases(dataread[,6]),6])
  tambstmx = max(subdata[,6])
  
  hambmx = max(dataread[complete.cases(dataread[,7]),7])
  hambstmx = max(subdata[,7])
  
  tambmn = min(dataread[complete.cases(dataread[,6]),6])
  tambstmn = min(subdata[,6])
  
  hambmn = min(dataread[complete.cases(dataread[,7]),7])
  hambstmn = min(subdata[,7])
  
  tsi1 = mean(dataread[complete.cases(dataread[,8]),8])
  tsi1min = min(dataread[complete.cases(dataread[,8]),8])
  tsi1max = max(dataread[complete.cases(dataread[,8]),8])
  
	tsi1st = mean(subdata[complete.cases(subdata[,8]),8])
  tsi1minst = min(subdata[complete.cases(subdata[,8]),8])
  tsi1maxst = max(subdata[complete.cases(subdata[,8]),8])
  
	ws = mean(dataread[complete.cases(dataread[,9]),9])
	sdws = sdp(dataread[complete.cases(dataread[,9]),9])
	covws = sdws*100/ws
	wsmax = max(dataread[complete.cases(dataread[,9]),9])
	wssh = mean(subdata[complete.cases(subdata[,9]),9])
	
	wd = mean(dataread[complete.cases(dataread[,10]),10])
	sdwd = sdp(dataread[complete.cases(dataread[,10]),10])
	covwd = sdwd*100/wd
  wdst = mean(subdata[complete.cases(subdata[,10]),10])

  gsirat = gsi2 / gsi1
  smprat = gsismp / gsi1
  
  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = rf(da),Gsi01 = rf(gsi1), Gsi02 = rf(gsi2),Smp = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf1(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         TMod01 = rf1(tsi1), GsiRat = rf3(gsirat), SpmRat = rf3(smprat),WindDir=rf0(wd),
												 WindDirSH=rf0(wdst),TModSH=rf1(tsi1st),TmodMax=rf1(tsi1max),TModMaxSH=rf1(tsi1maxst),TModMin=rf1(tsi1min),TModMinST=rf1(tsi1minst),
												 WindSpeedMean=rf1(ws),WindSpeedMax=rf1(wsmax),WindSpeedMeanSH=rf1(wssh),
												 SDWindSpd = rf(sdws), CovWindSpd=rf1(covws), SDWindDir=rf(sdwd),CovWindDir=rf1(covwd),
												 DA=rf1(daPerc),stringsAsFactors=F)
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi01 = as.character(datawrite[1,3]),Gsi02 = as.character(datawrite[1,4]),Smp = as.character(datawrite[1,5]),
             Tamb = as.character(datawrite[1,6]),TambSH = as.character(datawrite[1,7]),Hamb = as.character(datawrite[1,12]),HambSH = as.character(datawrite[,13]))
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[724]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-724S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-724S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("724","SG-724S",days[z])
			
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
