rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
INSTCAP = c(819,410.80)
DAYSACTIVE = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}

fetchGSIData = function(date,wait)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[SG-724S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[SG-724S] ',date,".txt",sep="")
	print(txtFileName)
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	if(wait && (!is.finite(gsiVal)))
	{
		print("Couldnt find file, sleeping for an hour")
		Sys.sleep(3600)
		gsiVal = fetchGSIData(date,0)
	}
	return(gsiVal)
}

prepareSumm = function(dataread,wait)
{
  da = nrow(dataread)
  daPerc = round(da/14.4,1)
	thresh = 5
	LastRead1 = LastTime1 = Eac11 = Eac21 = Eac31 =NA
  LastRead2 = LastTime2 = Eac12 = Eac22 = Eac32=NA
	
	date = substr(as.character(dataread[1,1]),1,10)
	Eac11 = as.numeric(dataread[,16])
	Eac11 = Eac11[complete.cases(Eac11)]
	
	{
	if(length(Eac11))
		Eac11 = sum(Eac11)/60
	else
		Eac11 = NA
	}

	Eac21 = as.numeric(dataread[,31])
	time = as.character(dataread[,1])
	time = time[complete.cases(Eac21)]
	Eac21 = Eac21[complete.cases(Eac21)]
	{
	if(length(Eac21))
	{
		LastRead1 = Eac21[length(Eac21)]
		Eac21 = (Eac21[length(Eac21)] - Eac21[1])
		LastTime1 = time[length(time)]
	}
	else
		Eac21 = NA
	}
	
	Eac31 = as.numeric(dataread[,32])
	Eac31 = Eac31[complete.cases(Eac31)]
	{
		if(length(Eac31))
			Eac31 = Eac31[length(Eac31)]
		else
			Eac31 = NA
	}
	
  Eac12 = as.numeric(dataread[,67])
	Eac12 = Eac12[complete.cases(Eac12)]
	
	{
	if(length(Eac12))
		Eac12 = sum(Eac12)/60
	else
		Eac12 = NA
	}
	
  Eac22 = as.numeric(dataread[,78])
	time = as.character(dataread[,1])
	time = time[complete.cases(Eac22)]
	Eac22 = Eac22[complete.cases(Eac22)]
	{
	if(length(Eac22))
	{
		LastRead2 = Eac22[length(Eac22)]
		Eac22 = (Eac22[length(Eac22)] - Eac22[1])
		LastTime2 = time[length(time)]
	}
	else
		Eac22 = NA
	}
	
	Eac32 = as.numeric(dataread[,79])
	Eac32 = Eac32[complete.cases(Eac32)]
	{
		if(length(Eac32))
			Eac32 = Eac32[length(Eac32)]
		else
			Eac32 = NA
	}
	
	Yld11 = Eac11/INSTCAP[1]
	Yld21 = Eac21/INSTCAP[1]
	IrrSG724 = fetchGSIData(date,wait)
	PR11 = Yld11*100/IrrSG724
	PR21 = Yld21*100/IrrSG724
	
  Yld12 = Eac12/INSTCAP[2]
	Yld22 = Eac22/INSTCAP[2]
	PR12 = Yld12*100/IrrSG724
	PR22 = Yld22*100/IrrSG724
  
  Eac1Tot = Eac11 + Eac12 
  Eac2Tot = Eac22 + Eac21 
  Yld1Tot = Eac1Tot /sum(INSTCAP)
  Yld2Tot = Eac2Tot /sum(INSTCAP)
  PR1Tot = Yld1Tot*100/IrrSG724
  PR2Tot = Yld2Tot*100/IrrSG724

	datawrite = data.frame(Date = date, PtsRec = da, DA = rf1(daPerc), 
  Eac11 = rf(Eac11), Eac21 = rf(Eac21),
	Yld11 = rf(Yld11), Yld21 = rf(Yld21), 
  GSiSG724 = IrrSG724, PR11 = rf1(PR11), PR21 = rf1(PR21),
	LastTime1 = LastTime1, LastRead1 = LastRead1, LastReadRec1 = Eac31, 
  Eac12 = rf(Eac12), Eac22 = rf(Eac22),
	Yld12 = rf(Yld12), Yld22 = rf(Yld22), 
  PR12 = rf1(PR12), PR22 = rf1(PR22),
	LastTime2 = LastTime2, LastRead2 = LastRead2, LastReadRec2 = Eac32,
  Eac1Tot = rf(Eac1Tot), Eac2Tot = rf(Eac2Tot), Yld1Tot = rf(Yld1Tot), 
  Yld2Tot=rf(Yld2Tot), PR1Tot=rf1(PR1Tot), PR2Tot=rf(PR2Tot),
  stringsAsFactors=F)

  datawrite 
}

rewriteSumm = function(datawrite)
{
  datawrite
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[726]"
pathwrite = "/home/admin/Dropbox/Second Gen/[SG-726S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-726S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread,0) #dont wait for history
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("726","SG-726S",days[z])
			if(x == 1 && y == 1 && z == 1)
				DOB = substr(days[z],10,19)
			DAYSACTIVE = DAYSACTIVE + 1
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
