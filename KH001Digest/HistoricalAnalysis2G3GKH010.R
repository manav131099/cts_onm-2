system('rm -R "/home/admin/Dropbox/Second Gen/[KH-001S]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/KH001Digest/KH001MailDigestFunctions.R')

path = '/home/admin/Dropbox/Cleantechsolar/1min/[714]'
writepath2G = '/home/admin/Dropbox/Second Gen/[KH-001S]'
checkdir(writepath2G)

writepath3G = writepath2G
checkdir(writepath3G)
DAYSALIVE = 0
LAST30DAYSEACPROD = vector('numeric',30)
LAST30DAYSEACGRIDIN = vector('numeric',30)
LAST30DAYSEACLOADCONS = vector('numeric',30)
LAST30DAYSLOADTOTS = vector('numeric',30)
MONTHEACPROD = 0
MONTHEACGRIDIN = 0
MONTHEACLOADCONS = 0
MONTHLOADTOTS = 0
DAYSTHISMONTH = 0
DAYSTHISMONTHAC = 0
GLOBEACPROD = 0
GLOBEACGRIDIN = 0
GLOBEACLOADCONS = 0
GLOBLOADTOTS = 0

idxvec = 1
years = dir(path)
for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
	  
		MONTHEACPROD = 0
		MONTHEACGRIDIN = 0
		MONTHEACLOADCONS = 0
		MONTHLOADTOTS = 0
		DAYSTHISMONTH = 0
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
		filenam = paste(substr(as.character(years[x]),3,4),substr(as.character(months[y]),6,7),sep="")
    writepath3Gfinal = paste(writepath2Gmon,"/[KH-001S] ",filenam,".txt",sep="")
    days = dir(pathmonths)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    DAYSTHISMONTHAC = monthDays(temp)
      for(t in 1 : length(days))
      {
        {
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
        }
        DAYSALIVE = DAYSALIVE + 1
				currdayw = gsub("714","KH-001S",days[t])
        writepath2Gfinal = paste(writepath2Gmon,"/",currdayw,sep="")
        readpath = paste(pathmonths,days[t],sep="/")
        df1 = secondGenData(readpath,writepath2Gfinal)
        tots = thirdGenData(writepath2Gfinal,writepath3Gfinal)
        LAST30DAYSEACPROD[[idxvec]] = tots[1]
				LAST30DAYSEACGRIDIN[[idxvec]] = tots[2]
				LAST30DAYSEACLOADCONS[[idxvec]] = tots[3]
				LAST30DAYSLOADTOTS[[idxvec]] = tots[4]
        MONTHEACPROD = MONTHEACPROD + tots[1]
        MONTHEACGRIDIN = MONTHEACGRIDIN + tots[2]
				MONTHEACLOADCONS = MONTHEACLOADCONS + tots[3]
				MONTHLOADTOTS = MONTHLOADTOTS+tots[4]
        DAYSTHISMONTH = DAYSTHISMONTH + 1
        GLOBEACPROD = GLOBEACPROD + tots[1]
        GLOBEACGRIDIN = GLOBEACGRIDIN + tots[2]
				GLOBEACLOADCONS = GLOBEACLOADCONS + tots[3]
				GLOBLOADTOTS = GLOBLOADTOTS+tots[4]
        idxvec = (idxvec + 1) %% 30
        if(idxvec == 0)
        {
          idxvec = 1
        }
      }
    }
}
print('Historical Analysis Done')
